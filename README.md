# Minimal ~/.emacs.d - Emacs Starter Kit with Better Defaults and Optimized Startup

The **minimal-emacs.d** repository offers a starter kit with improved Emacs defaults and optimized startup, designed to serve as a robust foundation for your vanilla Emacs configuration and enhance your overall Emacs experience.

This repo is a fork of **[minimal-emacs.d](https://github.com/jamescherti/minimal-emacs.d)** and `main` is supposed to stay synced with original repo's `main` branch.

I make my own customization in `my-main` branch.

