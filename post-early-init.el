;;; post-early-init.el --- This file is loaded after early-init.el but before init.el -*- no-byte-compile: t; lexical-binding: t; -*-

;; recommendation from https://github.com/progfolio/elpaca/blob/master/doc/early-init.el
;; and
;; 
(setq package-enable-at-startup nil)

(load custom-file)

(provide 'post-early-init)

;;; post-early-init.el ends here


