;;; post-init.el --- This file is loaded after init.el -*- no-byte-compile: t; lexical-binding: t; -*-

;;;; Need to update and load org-mode before loading `config.org`

(use-package org :ensure t)
(elpaca-wait)

;; Load the config
(org-babel-load-file (concat minimal-emacs-user-directory "config.org"))


(provide 'post-init)

;;; post-init.el ends here
