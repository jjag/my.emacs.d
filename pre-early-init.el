;;; pre-early-init.el --- This file is loaded before early-init.el -*- no-byte-compile: t; lexical-binding: t; -*-

;;;; Reducing clutter in ~/.emacs.d by redirecting files to ~/emacs.d/var/
;;;; https://github.com/jamescherti/minimal-emacs.d?tab=readme-ov-file#reducing-clutter-in-emacsd-by-redirecting-files-to-emacsdvar

(setq minimal-emacs-user-directory user-emacs-directory)
(setq minimal-emacs-var-dir
      (expand-file-name "var/" minimal-emacs-user-directory))
(setq user-emacs-directory minimal-emacs-var-dir)


(provide 'pre-early-init)

;;; pre-early-init.el ends here
